#!/usr/bin/env bash

# Generations - a game of multiple lifetimes
# Copyright (C) 2024  András Németh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

echo '========================================'
echo 'Starting build ...'
echo '----------------------------------------'

STACK_SIZE=1048576 #  1 MiB
HEAP_SIZE=67108864 # 64 MiB

echo '========================================'
echo 'Building Odin source to object file ...'
echo '----------------------------------------'

odin build src \
  -target=freestanding_wasm32 \
  -build-mode:obj \
  -extra-linker-flags:--static \
  -no-entry-point \
  -o:speed \
  -strict-style \
  -out="build-obj/odin.out" \
  || exit

echo '========================================'
echo 'Setting up emscripten SDK ...'
echo '----------------------------------------'

source ../../../git-ref/github/emscripten-core/emsdk/emsdk_env.sh || exit

echo '========================================'
echo 'Activating latest emscripten SDK ...'
echo '----------------------------------------'

emsdk activate latest || exit

echo '========================================'
echo 'Changing directory ...'
echo '----------------------------------------'

pushd build-wasm || exit

echo '========================================'
echo 'Compiling WASM with emcc ...'
echo '----------------------------------------'

emcc -o index.html \
  ../src/main.c \
  ../build-obj/odin.wasm.o \
  ../lib/raylib-include/libraylib.a \
  -O0 \
  -Wall -Wjs-compiler \
  -D WEB_BUILD \
  -D PLATFORM_WEB \
  -s WASM=1 \
  -s USE_GLFW=3 \
  -s GL_ENABLE_GET_PROC_ADDRESS \
  -s STACK_SIZE=$STACK_SIZE \
  -s TOTAL_MEMORY=$HEAP_SIZE \
  -s ERROR_ON_UNDEFINED_SYMBOLS=0 \
  -s LLD_REPORT_UNDEFINED \
  || exit

echo '========================================'
echo 'Changing directory back ...'
echo '----------------------------------------'

popd || exit

echo '========================================'
echo 'Linking files to web embed view ...'
echo '----------------------------------------'

rm web-embed/index.js
rm web-embed/index.wasm

ln -s ../build-wasm/index.js web-embed/index.js
ln -s ../build-wasm/index.wasm web-embed/index.wasm

echo '========================================'
echo 'Zipping web files ...'
echo '----------------------------------------'

zip \
  --no-dir-entries \
  --junk-paths \
  generations.zip \
  web-embed/index.html \
  build-wasm/index.js \
  build-wasm/index.wasm \
  || exit

echo '========================================'
echo 'Done.'
