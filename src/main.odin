// Generations - a game of multiple lifetimes
// Copyright (C) 2024  András Németh
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import "base:runtime"
import "core:mem"

// import rl "vendor:raylib"
import rl "vendor_raylib_mod"

foreign import "odin_env"

TITLE :: "Generations"
WIDTH  :: 800
HEIGHT :: 600
FPS_TARGET :: 60

frame: u64 = 0

ctx: runtime.Context

tempAllocatorData: [mem.Megabyte * 4]byte
tempAllocatorArena: mem.Arena

mainMemoryData: [mem.Megabyte * 16]byte
mainMemoryArena: mem.Arena

@(export, link_name="init")
init :: proc "contextless" () {
    ctx = runtime.default_context()
    context = ctx

    mem.arena_init(&mainMemoryArena, mainMemoryData[:])
    mem.arena_init(&tempAllocatorArena, tempAllocatorData[:])

    ctx.allocator      = mem.arena_allocator(&mainMemoryArena)
    ctx.temp_allocator = mem.arena_allocator(&tempAllocatorArena)

    context = ctx

    // Test the allocator works
    x := new(i64)
    assert(x != nil)

    rl.InitWindow(WIDTH, HEIGHT, TITLE)
    rl.SetTargetFPS(FPS_TARGET)
}

@(export, link_name="deinit")
deinit :: proc "contextless" () {
    rl.CloseWindow()
}

@(export, link_name="step")
step :: proc "contextless" () {
    context = ctx
    update()
}

update :: proc() {
    free_all(context.temp_allocator)

    pos_x: i32 = WIDTH / 2 - 90
    pos_y: i32 = HEIGHT / 2 - 20
    font_size: i32 = 24

    rl.BeginDrawing()
    defer rl.EndDrawing()

    rl.ClearBackground(rl.WHITE)

    rl.DrawText(TITLE, pos_x, pos_y, font_size, rl.LIGHTGRAY)
}
