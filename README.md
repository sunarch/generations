Generations
===========

Experimental game about improving and changing throughout generations

### Acknowledgements

- **Odin** by [Ginger Bill](https://github.com/gingerBill)
  used under the [BSD-3-Clause license](https://github.com/odin-lang/Odin/blob/master/LICENSE)
  - Odin [homepage](https://odin-lang.org),
    [GitHub](https://github.com/odin-lang/Odin)
- **Raylib** by Ramon Santamaria ([@raysan5](https://github.com/raysan5)) and contributors
  used under the [zlib License](https://github.com/raysan5/raylib/blob/master/LICENSE)
  - Raylib [homepage](https://www.raylib.com),
    [GitHub](https://github.com/raysan5/raylib)
  - Odin vendor library [pkg page](https://pkg.odin-lang.org/vendor/raylib/),
    [GitHub](https://github.com/odin-lang/Odin/tree/master/vendor/raylib)
  - Documentation in the repo wiki: [Working for Web (HTML5)](https://github.com/raysan5/raylib/wiki/Working-for-Web-(HTML5))
- **[Itch.io](https://itch.io)**
  - Documentation: [Uploading HTML5 games](https://itch.io/docs/creators/html5)
- The following webpages helped me to figure out the compilation process:
  - r/raylib - [Is it possible to make WASM game with Raylib using Oidn language?](https://www.reddit.com/r/raylib/comments/18koy03/is_it_possible_to_make_wasm_game_with_raylib/)
    by user [Nefrace](https://www.reddit.com/user/Nefrace/)
    and answers by them and user [charles__l](https://www.reddit.com/user/charles__l/)
    - GitHub: [charles-l/zig-odin-raylib-template](https://github.com/charles-l/zig-odin-raylib-template)
    - GitHub: [Caedo/raylib_wasm_odin](https://github.com/Caedo/raylib_wasm_odin)
  - GitHub Gist: [Odin language simple WebAsembly setup](https://gist.github.com/sortofsleepy/603e70468f0b33c56e220df698451ce6)
    by user [sortofsleepy](https://gist.github.com/sortofsleepy)
  - GitHub: [NHDaly/sdl-wasm-odin](https://github.com/NHDaly/sdl-wasm-odin)
  - GitHub: [GoldenbergDaniel/OdinWasmExample](https://github.com/GoldenbergDaniel/OdinWasmExample)
    
### License

[GNU AGPLv3](https://www.gnu.org/licenses/agpl-3.0.html)

```
Generations - a game of multiple lifetimes
Copyright (C) 2024  András Németh

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
