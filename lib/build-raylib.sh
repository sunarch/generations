#!/usr/bin/env bash

# Generations - a game of multiple lifetimes
# Copyright (C) 2024  András Németh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

echo '========================================'
echo 'Starting build ...'
echo '----------------------------------------'

echo '========================================'
echo 'Setting up emscripten SDK ...'
echo '----------------------------------------'

source ../../../../git-ref/github/emscripten-core/emsdk/emsdk_env.sh || exit


echo '========================================'
echo 'Activating latest emscripten SDK ...'
echo '----------------------------------------'

emsdk activate latest || exit

echo '========================================'
echo 'Removing previous output objects and archive ...'
echo '----------------------------------------'

rm raylib-include/libraylib.a
rm raylib/src/*.o
rm raylib/src/libraylib.a

echo '========================================'
echo 'Changing directory ...'
echo '----------------------------------------'

pushd raylib/src || exit

echo '========================================'
echo 'Checking out Git tag for Raylib 5.0 ...'
echo '----------------------------------------'

git checkout 5.0

echo '========================================'
echo 'Building Raylib for WASM ...'
echo '----------------------------------------'

make PLATFORM=PLATFORM_WEB GRAPHICS=GRAPHICS_API_OPENGL_ES3 -B

echo '========================================'
echo 'Copying compiled archive ...'
echo '----------------------------------------'

cp ./libraylib.a ../../raylib-include/

echo '========================================'
echo 'Changing directory back ...'
echo '----------------------------------------'

popd || exit

echo '========================================'
echo 'Done.'
